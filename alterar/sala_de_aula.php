<html>
<head>
	<?php include("../iniciar-sessao_portal.php"); ?>
	<title>Escola Inteligente - Cadastro</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<script src="javascript.js" ></script>
</head>
<body>
<center>

	<div id="topo">
		<div id="topo-limite">
			<img src="../imagens/alerta2.png" id="icone-auto-falante">
			<font id="texto-topo">
				Altere alguma sala de aula
			</font>
			<a href="../index.php"><input type="submit" value="Voltar ao inicio" id="botao-medio"></a>
		</div>
	</div>
	
	<div id="cadastro-instituicao">
	
		<form method="post" action="sala_de_aula.php?acao=buscarSala">
			<input type="text" id="campo-buscador" name="buscador" placeholder="Digite o nome da sala">
			<input type="submit" id="botao-pequeno" value="Buscar">	
		</form>
		

		
		<?php 
		include("../conectar_banco.php");
		
		ini_set('display_errors', 0 );
		error_reporting(0);

		
		if($_REQUEST["acao"] == 'buscarSala'){

			$sql = "SELECT * FROM sala_de_aula WHERE nome_instituicao = '".$_SESSION["nome_instituicao"]."' AND sala_de_aula = '".$_POST["buscador"]."'";
			$resultado = mysql_query($sql);
			if(!mysql_fetch_array($resultado)){
				echo "<font id='fonte-nao-encontrado-e-pesquise'>nenhuma sala de aula encontrada</font>";				
			}
			$resultado = mysql_query($sql);//se retirar esta linha o buscador nao funciona direito por causa do if acima
		}else{
			echo "<font id='fonte-nao-encontrado-e-pesquise'>Altere os dados da sala de aula utilizando o buscador</font>";
		}		
		
		while($atributo = mysql_fetch_array($resultado)){

			echo "<form method='post' action='alterar.php?acao=alterarSala&id=".$atributo["id"]."'>";
			echo "<div id='quadrado-alterar'>";
				echo "<div id='quadrado-alterar-limite'>";
					echo "<input type='text' placeholder='Nome da sala de aula' value='".$atributo["sala_de_aula"]."' required maxlength='50' name='sala_de_aula' pattern='[1-9a-zA-Zãéºªíóõçô\s]+$' id='campo-grande'>";
					echo "<input type='text' placeholder='Valor de cada aluno' value='".$atributo["valor_aluno"]."' name='valor_aluno' pattern='[0-9\s]+$'  maxlength='5' required id='campo-grande'>";
				echo "</div>";
			echo "</div>";
			echo "<input type='submit' id='botao-pequeno' value='Alterar'>";	
			echo "</form>";
			
		}
		
		?>	
	
	</div>

</center>
</body>
</html>