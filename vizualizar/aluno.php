<html>
<head>
	<?php include("../iniciar-sessao_portal.php"); ?>
	<title>Escola Inteligente - Cadastro</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<script src="javascript.js" ></script>
</head>
<body>
<center>

	<div id="topo">
		<div id="topo-limite">
			<img src="../imagens/lupa.png" id="icone-auto-falante">
			<font id="texto-topo">
				Vizualize todos os alunos da instituição
			</font>
			<a href="../index.php"><input type="submit" value="Voltar ao inicio" id="botao-medio"></a>
		</div>
	</div>
	
	<div id="cadastro-instituicao">
	
			
		<form method="post" action="aluno.php?acao=buscarAluno">
			<input type="text" id="campo-buscador" name="buscador" placeholder="Busque o aluno pelo nome, email ou série">
			<input type="submit" id="botao-pequeno" value="Buscar">	
		</form>
		
		<a href="PDFAluno.php"><input type="submit" value="Gerar pdf" id="botao-grande"></a>	

		<table border="0" id="tabela-vizualizar">
			<tr id="texto-titulo-tabela-vizualizar">
				<td>nome do aluno</td>
				<td>email</td>
				<td>série</td>
				<td>telefone movel</td>
				<td>telefone fixo</td>
				<td>Ver detalhes</td>			
			</tr>
			
			<?php 
			include("../conectar_banco.php");
			
			ini_set('display_errors', 0 );
			error_reporting(0);

			
			$resultado = mysql_query("SELECT * FROM aluno WHERE nome_instituicao = '".$_SESSION["nome_instituicao"]."'");

			if($_REQUEST["acao"] == 'buscarAluno'){

				$sql = "SELECT * FROM aluno WHERE nome_instituicao = '".$_SESSION["nome_instituicao"]."' AND nome_completo = '".$_POST["buscador"]."' OR email = '".$_POST["buscador"]."' OR sala_de_aula = '".$_POST["buscador"]."' OR ser_chamado = '".$_POST["buscador"]."'";
				$resultado = mysql_query($sql);
				
			}

			
			while($atributo = mysql_fetch_array($resultado)){
				
			echo "<tr id='texto-tabela-vizualizar'>";
			echo "<td>".$atributo["nome_completo"]."</td>";
			echo "<td>".$atributo["email"]."</td>";
			echo "<td>".$atributo["sala_de_aula"]."</td>";
			echo "<td>".$atributo["telefone_movel"]."</td>";
			echo "<td>".$atributo["telefone_fixo"]."</td>";
			echo "<td><a href='detalhesAluno.php?email=".$atributo["email"]."'><div id='icone-detalhes'></div></a></td>";			
			echo "</tr>";
				
			}
			
			?>
			
		</table>
		
	
	</div>

</center>
</body>
</html>