<html>
<head>
	<?php include("../iniciar-sessao_portal.php"); ?>
	<title>Escola Inteligente - Cadastro</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<script src="javascript.js" ></script>
</head>
<body>
<center>

	<div id="topo">
		<div id="topo-limite">
			<img src="../imagens/lupa.png" id="icone-auto-falante">
			<font id="texto-topo">
				Vizualize, aluge, renove e devolva os livros da sua biblioteca online
			</font>
			<a href="../index.php"><input type="submit" value="Voltar ao inicio" id="botao-medio"></a>
		</div>
	</div>
	
	<div id="cadastro-instituicao">
	
	<form method="post" action="biblioteca_online.php?acao=buscarLivro">
		<input type="text" id="campo-buscador" name="buscador" placeholder="Busque o livro pelo titulo, editora, autor ou codigo">
		<input type="submit" id="botao-pequeno" value="Buscar">	
	</form>
	
	<a href="PDFlivros.php"><input type="submit" value="Gerar pdf" id="botao-grande"></a>	
	<a href="livrosAlugados.php"><input type="submit" value="Dar baixa em livro" style="margin-right:10px;width:200px;" id="botao-grande"></a>	

	<table border="0" id="tabela-vizualizar">
		<tr id="texto-titulo-tabela-vizualizar">
			<td>Titulo</td>
			<td>Editora</td>
			<td>Autor</td>
			<td>Descrição</td>
			<td>Codigo</td>
			<td>Status</td>
			<td>Alugar</td>			
		</tr>
		
		<?php 
		include("../conectar_banco.php");
		
		ini_set('display_errors', 0 );
		error_reporting(0);

		
		$resultado = mysql_query("SELECT * FROM livro WHERE escola = '".$_SESSION["nome_instituicao"]."'");

		if($_REQUEST["acao"] == 'buscarLivro'){

			$sql = "SELECT * FROM livro WHERE escola = '".$_SESSION["nome_instituicao"]."' AND titulo = '".$_POST["buscador"]."' OR editora = '".$_POST["buscador"]."' OR autor = '".$_POST["buscador"]."' OR codigo = '".$_POST["buscador"]."'";
			$resultado = mysql_query($sql);
			
		}

		
		while($atributo = mysql_fetch_array($resultado)){
			
			echo "<tr id='texto-tabela-vizualizar'>";
			echo "<td>".$atributo["titulo"]."</td>";
			echo "<td>".$atributo["editora"]."</td>";
			echo "<td>".$atributo["autor"]."</td>";
			echo "<td>".$atributo["descricao"]."</td>";
			echo "<td>".$atributo["codigo"]."</td>";
			if($atributo["status"] == 'livre'){
				echo "<td>".$atributo["status"]."</td>";
			}else{
				echo "<td style='color:red;'>".$atributo["status"]."</td>";
			}
			echo "<td><a href='alugarLivro.php?codigo=".$atributo["codigo"]."'><div id='icone-alugar'></div></a></td>";			
			echo "</tr>";			
				
		}
		
		?>
		
	</table>
	
	</div>

</center>
</body>
</html>