<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="estilo.css">

<center>
	<div id="area-error-login">
		<img src="imagens/alerta.png" id="alerta">
		<font id="texto-erro-mensagem-login"><?php echo $_REQUEST['mensagem']; ?></font>		
		<form method="post" action="validar-login.php?acao=entrarADM">
			<input type="text" placeholder="Digite o seu email" name="email" required pattern="[a-zA-Z0-9._%+-ãéíóô]+@[a-zA-Z0-9.-ãéíóô]+\.[a-z]{2,4}$" id="campo-texto-error-login">
			<input type="password" placeholder="Digite a senha" name="senha" required pattern="[a-zA-Z0-9ãéíóô\s]+$" id="campo-senha-error-login">
			<input type="submit" value="Entrar" id="botao-error">
		</form>
		<div id="escolhas-error">
			<a href="#" id="opcao-error">esqueceu sua senha!</a>
			<a href="index.php" id="opcao-error">voltar ao inicio</a>		
			<a href="cadastro/instituicao.html" id="opcao-error">quero me cadastrar!</a>
		</div>
	</div>
</center>
