<?php
session_start();

ini_set('display_errors', 0 );
error_reporting(0);

	if($_SESSION["autentificado"] == TRUE){
		header("Location: ADM/login.php");
		exit();
	}
	if($_SESSION["autentificacao"] == TRUE){
		header("Location: /ProjetoEducacional/".$_SESSION["tipoPessoa"]."/login.php");
		exit();
	}	
	
	
?>
<html>
<head>

	<title>Escola Inteligente</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<script src="javascript.js" ></script>	
</head>

<body>
<center>
	<div id="PrimeiraArea">
		<div id="topo">
			<div id="topo-limite">
				<img src="imagens/logo-escola.png" id="imagem-logo">
				<div id="ambiente-login">
					<form method="post" action="validar-login.php?acao=entrarADM">
						<input type="text" placeholder="Digite o seu email" name="email" required pattern="[a-zA-Z0-9._%+-ãéíóô]+@[a-zA-Z0-9.-ãéíóô]+\.[a-z]{2,4}$" id="campo-texto-instituicao">
						<input type="password" placeholder="Digite a senha" pattern="[a-zA-Z0-9ãéíóô\s]+$" name="senha" required id="campo-texto">
						<input type="submit" value="Entrar" id="botao-medio">
					</form>
				</div>
			</div>
		</div>
		<div id="Informacoes-PrimeiraArea">
			<div id="Informacoes-PrimeiraArea-limite">
				<font id="titulo-texto">A escolha inteligente faz<br> a sua</font><br>
				<font id="titulo-texto2"><b>escola inteligente!</b></font><br><br>
				<a href="cadastro/instituicao.html"><input type="submit" id="botao-grande" value="Cadastre-se!"></a>
			</div>
		</div>
	</div>

	<div id="SegundaArea">
		<div id="SegundaArea-limite">
			<font id="titulo-segundaarea">O que é a escola inteligente?</font>
			<font id="texto-segundaarea">Escola inteligente é um site afim de oferecer suporte a escolas de ensino medio e fundamental com um portal educacional totalmente editavel,
			esse portal uma vez criado ira fazer com que todas as tarefas de sua escola seja automatizado isto ira trazer para sua escola varias vantagens como:</font>
			<div id="vantagens">
				<div id="topico-vantagem">
					<img src="imagens/icone-portal.png" id="img-icones"><br><br>
					<font id="texto-topico-vantagem">Crie um portal educacional para sua escola e tenha 100% controle da sua escola</font>
				</div>
				<div id="topico-vantagem">
					<img src="imagens/icone-grafico.png" id="img-icones"><br><br>
					<font id="texto-topico-vantagem">A instituição tera relatorios em tempo real dos orcamentos de contas a pagar e receber da escola.</font>
				</div>
				<div id="topico-vantagem">
					<img src="imagens/icone-professor.png" id="img-icones"><br><br>
					<font id="texto-topico-vantagem"> Facilite a vida do seu Professor com a caderneta online</font>
				</div>
				<div id="topico-vantagem">
					<img src="imagens/icone-boleto.png" id="img-icones"><br><br>
					<font id="texto-topico-vantagem">Seus alunos poderão realizar pagamentos quando quiserem e onde estiverem  </font>
				</div>
				<div id="topico-vantagem">
					<img src="imagens/icone-sala.png" id="img-icones"><br><br>
					<font id="texto-topico-vantagem">Crie saulas de aula online e gerencie as turmas em tempo real</font>
				</div>
				<div id="topico-vantagem">
					<img src="imagens/icone-livros.png" id="img-icones"><br><br>
					<font id="texto-topico-vantagem">Com a função biblioteca online a instituição gerencia melhor a biblioteca com o seu controle de estoque.</font>
				</div>
				
				<a href="vantagens.html" id="texto-saiba-mais-vantagens">
					Saiba mais vantagens
				</a>
				
			</div>
		</div>
	</div>
	
	<div id="TerceiraArea">
		<div id="TerceiraArea-limite">
			<div id="rodape-divisao">
				<font id="titulo-texto-rodape">Mapa do site</font><br><br>
				<table border="0" style="float:left;">
					<tr><td><a href="index.php" id="texto-rodape">Inicio</a></td></tr>
					<tr><td><a href="vantagens.html" id="texto-rodape">Vantagens</a></td></tr>
				</table>				
			</div>		
			<div id="rodape-divisao">
				<table border="0" style="float:left;">
					<tr><td><a id="texto-rodape" href="contato.php?tipo=mensagem">Entre em contato conosco</a></td></tr>
					<tr><td><a id="texto-rodape" href="contato.php?tipo=ideia">Mande-nos uma ideia</a></td></tr>
					<tr><td><a id="texto-rodape" href="contato.php?tipo=trabalho">Trabalhe conosco</a></td></tr>
					<tr><td><a id="texto-rodape" href="contato.php?tipo=error">Relate algum erro</a></td></tr>
				</table>
			</div>
			<div id="rodape-divisao">		
				<font id="titulo-texto-rodape">Nosso contato</font><br><br>
				<table border="0" style="float:left;">
					<tr><td><a href="index.php" id="texto-rodape">faleconosco@escolainteligente.com</a></td></tr>
					<tr><td><a href="vantagens.php" id="texto-rodape">(81) 999709-9497</a></td></tr>
				</table>
			</div>			
		</div>
	</div>
</center>
</body>
</html>
