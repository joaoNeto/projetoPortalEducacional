-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Máquina: 127.0.0.1
-- Data de Criação: 21-Jul-2016 às 03:17
-- Versão do servidor: 5.5.32
-- versão do PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `escola_inteligente`
--
CREATE DATABASE IF NOT EXISTS `escola_inteligente` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `escola_inteligente`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `aluno`
--

CREATE TABLE IF NOT EXISTS `aluno` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nome_instituicao` varchar(100) NOT NULL,
  `sala_de_aula` varchar(50) DEFAULT NULL,
  `nome_completo` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefone_movel` varchar(20) NOT NULL,
  `telefone_fixo` varchar(20) NOT NULL,
  `senha` varchar(20) NOT NULL,
  `ser_chamado` varchar(12) NOT NULL,
  `foto_perfil` varchar(100) NOT NULL,
  `estado` text NOT NULL,
  `cep` int(20) NOT NULL,
  `cidade` text NOT NULL,
  `bairro` varchar(50) NOT NULL,
  `rua` varchar(50) NOT NULL,
  `numero` int(5) NOT NULL,
  `complemento` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `aluno`
--

INSERT INTO `aluno` (`id`, `nome_instituicao`, `sala_de_aula`, `nome_completo`, `email`, `telefone_movel`, `telefone_fixo`, `senha`, `ser_chamado`, `foto_perfil`, `estado`, `cep`, `cidade`, `bairro`, `rua`, `numero`, `complemento`) VALUES
(1, 'Escola Técnica José Humberto de Moura Cavalcanti', '8º série', 'Emilio surita da silva', 'emiliozurita@hotmail.com', '', '', '123123', 'Emilio surit', '../imagens/foto-perfil/foto-padrao.png', 'Estado', 0, '', '', '', 0, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacao`
--

CREATE TABLE IF NOT EXISTS `avaliacao` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `enviado` varchar(250) NOT NULL,
  `titulo` varchar(250) NOT NULL,
  `para` varchar(20) NOT NULL,
  `views` int(10) NOT NULL,
  `data` varchar(20) NOT NULL,
  `nome_instituicao` varchar(100) NOT NULL,
  `sala_de_aula` varchar(50) NOT NULL,
  `aluno` int(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionario`
--

CREATE TABLE IF NOT EXISTS `funcionario` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nome_completo` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefone_movel` varchar(20) NOT NULL,
  `telefone_fixo` varchar(20) NOT NULL,
  `senha` varchar(20) NOT NULL,
  `ser_chamado` varchar(12) NOT NULL,
  `estado` text NOT NULL,
  `cep` varchar(20) NOT NULL,
  `cidade` text NOT NULL,
  `bairro` varchar(50) NOT NULL,
  `rua` varchar(50) NOT NULL,
  `numero` int(5) NOT NULL,
  `complemento` varchar(50) NOT NULL,
  `nome_cargo` varchar(20) NOT NULL,
  `salario` int(10) NOT NULL,
  `r_pe` int(1) NOT NULL,
  `r_sa` int(1) NOT NULL,
  `r_ga` int(1) NOT NULL,
  `r_gf` int(1) NOT NULL,
  `r_bo` int(1) NOT NULL,
  `r_po` int(1) NOT NULL,
  `foto_perfil` varchar(100) NOT NULL,
  `nome_instituicao` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `instituicao`
--

CREATE TABLE IF NOT EXISTS `instituicao` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nome_completo` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `tel_movel` varchar(20) NOT NULL,
  `tel_fixo` varchar(20) NOT NULL,
  `senha` varchar(20) NOT NULL,
  `ser_chamado` varchar(12) NOT NULL,
  `nome_instituicao` varchar(100) NOT NULL,
  `cnpj` int(20) NOT NULL,
  `inscr_estadual` int(20) NOT NULL,
  `data_fundacao` varchar(15) NOT NULL,
  `telefone_movel` varchar(20) NOT NULL,
  `telefone_fixo` varchar(20) NOT NULL,
  `estado` text NOT NULL,
  `cep` int(10) NOT NULL,
  `cidade` text NOT NULL,
  `bairro` text NOT NULL,
  `rua` text NOT NULL,
  `numero` int(5) NOT NULL,
  `complemento` text NOT NULL,
  `foto_perfil` varchar(100) NOT NULL,
  `r_pe` int(1) NOT NULL,
  `r_sa` int(1) NOT NULL,
  `r_ga` int(1) NOT NULL,
  `r_gf` int(1) NOT NULL,
  `r_bo` int(1) NOT NULL,
  `r_po` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `instituicao`
--

INSERT INTO `instituicao` (`id`, `nome_completo`, `email`, `tel_movel`, `tel_fixo`, `senha`, `ser_chamado`, `nome_instituicao`, `cnpj`, `inscr_estadual`, `data_fundacao`, `telefone_movel`, `telefone_fixo`, `estado`, `cep`, `cidade`, `bairro`, `rua`, `numero`, `complemento`, `foto_perfil`, `r_pe`, `r_sa`, `r_ga`, `r_gf`, `r_bo`, `r_po`) VALUES
(1, 'João José de Sousa Neto', 'mundodosnerds.com.br@hotmail.com', '(81)97099497', '(81)96635575', '123asd123asd', 'João Neto', 'Escola Técnica José Humberto de Moura Cavalcanti', 2147483647, 2147483647, '12/12/2015', '(81)97099497', '(81)97099497', 'pernambuco', 55715000, 'Feira Nova', 'Centro', 'Rua tres irmãs', 2, 'perto aos correios', '../imagens/foto-perfil/foto-padrao.png', 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `livro`
--

CREATE TABLE IF NOT EXISTS `livro` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(50) NOT NULL,
  `editora` varchar(50) NOT NULL,
  `autor` varchar(50) NOT NULL,
  `descricao` varchar(50) NOT NULL,
  `codigo` varchar(17) NOT NULL,
  `status` text NOT NULL,
  `escola` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `livro`
--

INSERT INTO `livro` (`id`, `titulo`, `editora`, `autor`, `descricao`, `codigo`, `status`, `escola`) VALUES
(1, 'O melhor do php', 'novatec', 'paulo roberto ', 'capa preta e branca', '12312.12312.31231', 'livre', 'Escola Técnica José Humberto de Moura Cavalcanti');

-- --------------------------------------------------------

--
-- Estrutura da tabela `livros_alugados`
--

CREATE TABLE IF NOT EXISTS `livros_alugados` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_livro` int(5) NOT NULL,
  `nome_completo` varchar(50) NOT NULL,
  `sala_de_aula` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `mensagem`
--

CREATE TABLE IF NOT EXISTS `mensagem` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `de` varchar(50) NOT NULL,
  `para` varchar(50) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `mensagem` varchar(500) NOT NULL,
  `data` varchar(20) NOT NULL,
  `lido` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `portal`
--

CREATE TABLE IF NOT EXISTS `portal` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `ativo` int(1) DEFAULT NULL,
  `nome_instituicao` varchar(100) NOT NULL,
  `endereco_portal` varchar(100) DEFAULT NULL,
  `tipo_portal` varchar(20) NOT NULL,
  `logo_escola` varchar(100) DEFAULT NULL,
  `plano_fundo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `professor`
--

CREATE TABLE IF NOT EXISTS `professor` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nome_completo` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `foto_perfil` varchar(100) NOT NULL,
  `telefone_movel` varchar(20) DEFAULT NULL,
  `telefone_fixo` varchar(20) DEFAULT NULL,
  `senha` varchar(20) NOT NULL,
  `ser_chamado` varchar(12) DEFAULT NULL,
  `estado` text,
  `cep` int(10) DEFAULT NULL,
  `cidade` text,
  `bairro` text,
  `rua` text,
  `numero` int(5) DEFAULT NULL,
  `complemento` text,
  `materia_lessionada` varchar(25) NOT NULL,
  `seg` int(1) DEFAULT NULL,
  `ter` int(1) DEFAULT NULL,
  `qua` int(1) DEFAULT NULL,
  `qui` int(1) DEFAULT NULL,
  `sex` int(1) DEFAULT NULL,
  `sab` int(1) DEFAULT NULL,
  `dom` int(1) DEFAULT NULL,
  `nome_instituicao` varchar(100) DEFAULT NULL,
  `salario` int(10) NOT NULL,
  `sala_de_aula` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `repositorio_questoes`
--

CREATE TABLE IF NOT EXISTS `repositorio_questoes` (
  `id_questao` int(10) NOT NULL AUTO_INCREMENT,
  `id_avalicao` int(10) NOT NULL,
  `nome` varchar(500) NOT NULL,
  `ordem` int(5) NOT NULL,
  `alternativa` varchar(500) NOT NULL,
  `resposta` varchar(2) NOT NULL,
  PRIMARY KEY (`id_questao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sala_de_aula`
--

CREATE TABLE IF NOT EXISTS `sala_de_aula` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sala_de_aula` varchar(50) NOT NULL,
  `max_aluno` int(5) NOT NULL,
  `nome_instituicao` varchar(100) NOT NULL,
  `valor_aluno` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `sala_de_aula`
--

INSERT INTO `sala_de_aula` (`id`, `sala_de_aula`, `max_aluno`, `nome_instituicao`, `valor_aluno`) VALUES
(1, '8º série', 100, 'Escola Técnica José Humberto de Moura Cavalcanti', 150);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
