<html>
<head>
	<?php include("../iniciar-sessao.php"); ?>
	<title>Escola Inteligente - Cadastro</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<script src="javascript.js" ></script>
</head>
<body>
<center>

	<div id="topo">
		<div id="topo-limite">
			<img src="../imagens/auto-falante.png" id="icone-auto-falante">
			<font id="texto-topo">
				Realize o cadastro do seu portal educacional
			</font>
			<a href="../index.php"><input type="submit" value="Voltar ao inicio" id="botao-medio"></a>
		</div>
	</div>
	
	<div id="cadastro-instituicao">
		<font id="texto-cadastro">Cadastre seu portal</font>

		<div id="etapa-cadastro">
		
			<font id="titulo-etapa-cadastro">Escolha algum portal para voce editar</font><br>
				
			<a href="cadastro.php?cadastro=portal&tipo=portal1">			
				<div id="tipoPortalEscolha" style="background-image:url('../imagens/portal01.png');">
				</div>
			</a>					
			
			<a href="cadastro.php?cadastro=portal&tipo=portal2">			
				<div id="tipoPortalEscolha" style="background-image:url('../imagens/portal02.png');">
				</div>
			</a>
			
		</div>	
		
	</div>

</center>
</body>
</html>