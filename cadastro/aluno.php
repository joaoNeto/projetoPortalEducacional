<html>
<head>
	<?php include("../iniciar-sessao_portal.php"); ?>
	<title>Escola Inteligente - Cadastro</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<script src="javascript.js" ></script>
</head>
<body>
<center>

	<div id="topo">
		<div id="topo-limite">
			<img src="../imagens/auto-falante.png" id="icone-auto-falante">
			<font id="texto-topo">
				Devido a política da empresa informe os dados abaixo por questões de segurança<br> para criar sua conta  agradecemos a compreensão
			</font>
			<a href="../index.php"><input type="submit" value="Voltar ao inicio" id="botao-medio"></a>
		</div>
	</div>
	
	<div id="cadastro-instituicao">
		<form method="post" name="formularioADM" action="cadastro.php?cadastro=aluno">
			<font id="texto-cadastro">Cadastre seu aluno</font>
			
			<div id="etapa-cadastro">
				<font id="titulo-etapa-cadastro">Sobre o seu aluno</font><br>
				<div id="area-questionario">
					<input type="text"  placeholder="Nome completo*" required maxlength="50" name="nome_completo" pattern="[a-zA-Zãéíóô\s]+$" id="campo-grande">
					<input type="text" placeholder="Email*" name="email" pattern="[a-zA-Z0-9._%+-ãéíóô]+@[a-zA-Z0-9.-ãéíóô]+\.[a-z]{2,4}$"  maxlength="50" required id="campo-grande">
					<input type="text" placeholder="Telefone movel" pattern="[0-9()-]+$" name="tel_movel" maxlength="20" id="campo-medio">
					<input type="text" placeholder="Telefone fixo" pattern="[0-9()-]+$" name="tel_fixo" maxlength="20" id="campo-medio">
					<input type="password" placeholder="Senha*" pattern="[a-zA-Z0-9ãéíóô\s]+$" required name="senha" maxlength="20" id="campo-medio">
					<input type="password" placeholder="Repita sua senha*" pattern="[a-zA-Z0-9ãéíóô\s]+$" name="repetir_senha" required maxlength="20" id="campo-medio">
					<input type="text" placeholder="Como gostaria de ser chamado" pattern="[a-zA-Z0-9ãéíóô\s]+$" maxlength="12" name="ser_chamado" id="campo-grande">
					<select id="campo-grande" name="sala_de_aula" >
						<option value="nao vinculado">Vincule aluno a alguma sala</option>
						<?php
						include("../conectar_banco.php");
						$sql = "SELECT * FROM sala_de_aula WHERE nome_instituicao = '".$_SESSION["nome_instituicao"]."'";
						$result = mysql_query($sql);
						while($atributo = mysql_fetch_array($result)){
							$sala = $atributo["sala_de_aula"];
							echo "<option value='$sala'>$sala</option>";
						}
						?>
					</select>
				</div>
				<img src="../imagens/icone-perfil2.png" id="icone-etapa-cadastro-instituicao">
			</div>
			<div id="serapar"></div>
			<div id="etapa-cadastro">
				<font id="titulo-etapa-cadastro">Localização do aluno</font><br>
				<div id="area-questionario">
					<select id="campo-medio" name="estado" >
						<option>Estado</option>
						<option value="acre">Acre (AC)</option>
						<option value="alagoas">Alagoas (AL)</option>
						<option value="amapa">Amapá (AP)</option>
						<option value="amazonas">Amazonas (AM)</option>
						<option value="bahia">Bahia (BA)</option>
						<option value="ceara">Ceará (CE)</option>
						<option value="distrito federal">Distrito Federal (DF)</option>
						<option value="espirito santo">Espírito Santo (ES)</option>
						<option value="goias">Goiás (GO)</option>
						<option value="maranhao">Maranhão (MA)</option>
						<option value="mato grosso">Mato Grosso (MT)</option>
						<option value="mato grosso do sul">Mato Grosso do Sul (MS)</option>
						<option value="minas gerais">Minas Gerais (MG)</option>
						<option value="para">Pará (PA) </option>
						<option value="paraiba">Paraíba (PB)</option>
						<option value="parana">Paraná (PR)</option>
						<option value="pernambuco">Pernambuco (PE)</option>
						<option value="piaui">Piauí (PI)</option>
						<option value="rio de janeiro">Rio de Janeiro (RJ)</option>
						<option value="rio grande do norte">Rio Grande do Norte (RN)</option>
						<option value="rio grande do sul">Rio Grande do Sul (RS)</option>
						<option value="rondonia">Rondônia (RO)</option>
						<option value="roraima">Roraima (RR)</option>
						<option value="santa catarina">Santa Catarina (SC)</option>
						<option value="sao paulo">São Paulo (SP)</option>
						<option value="sergipe">Sergipe (SE)</option>
						<option value="tocantins">Tocantins (TO)</option>
					</select>
					<input type="text" placeholder="CEP" name="cep" pattern="[0-9]+$" maxlength="20" id="campo-medio">
					<input type="text" placeholder="Cidade" name="cidade" maxlength="50" pattern="[áãéíóôa-zA-Z\s]+$" id="campo-medio">
					<input type="text" placeholder="Bairro" name="bairro" pattern="[áãéíóôa-zA-Z\s]+$" maxlength="50" id="campo-medio">
					<input type="text" placeholder="Rua" name="rua" pattern="[áãéíóôa-zA-Z\s]+$"  maxlength="50" id="campo-medio">
					<input type="text" placeholder="Numero" name="numero" pattern="[0-9]+$" maxlength="5" id="campo-medio">			
					<input type="text" placeholder="Complemento" pattern="[a-zA-Z1-9\s]+$" name="complemento" maxlength="50" id="campo-grande">				
				
				</div>
				<img src="../imagens/icone-localizacao.png" id="icone-etapa-cadastro-instituicao2">
			</div>

			<input type="submit" value="Cadastrar!" onclick="validarFormularioADM()" id="botao-grande">
		</form>
	</div>

</center>
</body>
</html>