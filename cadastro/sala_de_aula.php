<html>
<head>
	<?php include("../iniciar-sessao_portal.php"); ?>
	<title>Escola Inteligente - Cadastro</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<script src="javascript.js" ></script>
</head>
<body>
<center>

	<div id="topo">
		<div id="topo-limite">
			<img src="../imagens/auto-falante.png" id="icone-auto-falante">
			<font id="texto-topo">
				Devido a política da empresa informe os dados abaixo por questões de segurança<br> para criar sua conta  agradecemos a compreensão
			</font>
			<a href="../index.php"><input type="submit" value="Voltar ao inicio" id="botao-medio"></a>
		</div>
	</div>
	
	<div id="cadastro-instituicao">
		<form method="post" name="formularioADM" action="cadastro.php?cadastro=sala">
			<font id="texto-cadastro">Sala de aula</font>
			
			<div id="etapa-cadastro">
				<font id="titulo-etapa-cadastro">Cadastre sua sala de aula</font><br>
				<div id="area-questionario">
					<input type="text" placeholder="Nome da sala de aula" required maxlength="50" name="sala_de_aula" pattern="[1-9a-zA-Zãéºªíóõçô\s]+$" id="campo-grande">
					<input type="text" placeholder="Quantidade maxima de alunos" name="max_aluno" pattern="[0-9\s]+$"  maxlength="5" required id="campo-grande">
					<input type="text" placeholder="Valor de cada aluno" name="valor_aluno" pattern="[0-9\s]+$"  maxlength="5" required id="campo-grande">					
				</div>
			</div>
			<!-- | Fazer um select de todos os alunos que pertemcem a esta escola e com sala de aula = null | -->
			<input type="submit" value="Cadastrar!" onclick="validarFormularioADM()" id="botao-grande">
		</form>
	</div>

</center>
</body>
</html>