<html>
<head>
	<?php include("../iniciar-sessao_portal.php"); ?>
	<title>Escola Inteligente - Cadastro</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<script src="javascript.js" ></script>
</head>
<body>
<center>

	<div id="topo">
		<div id="topo-limite">
			<img src="../imagens/auto-falante.png" id="icone-auto-falante">
			<font id="texto-topo">
				Devido a política da empresa informe os dados abaixo por questões de segurança<br> para criar sua conta  agradecemos a compreensão
			</font>
			<a href="../index.php"><input type="submit" value="Voltar ao inicio" id="botao-medio"></a>
		</div>
	</div>
	
	<div id="cadastro-instituicao">
		<form method="post" name="formularioADM" action="cadastro.php?cadastro=livro">
			<font id="texto-cadastro">Biblioteca online</font>
			
			<div id="etapa-cadastro">
				<font id="titulo-etapa-cadastro">Cadastre seu livro</font><br>
				<div id="area-questionario">
					<input type="text" placeholder="Titulo do livro" required maxlength="50" name="titulo" pattern="[1-9a-zA-Zãéíóõçô\s]+$" id="campo-grande">
					<input type="text" placeholder="Editora" name="editora" pattern="[1-9a-zA-Zãéíóô\s]+$"  maxlength="50" required id="campo-grande">
					<input type="text" placeholder="Autor" pattern="[1-9a-zA-Zãéíóô\s]+$" name="autor" maxlength="50" id="campo-grande">
					<input type="text" placeholder="Descrição do livro" pattern="[1-9a-zA-Zãéíóô\s]+$" name="descricao" maxlength="50" id="campo-grande">
					<table id="tabela-classificacao-livro">
						<tr>
							<td>Classificação:</td>
							<td><input type="text" id="campo-pequeno" name="class1" pattern="[1-9a-zA-Z\s]+$" maxlength="5"></td>
							<td><input type="text" id="campo-pequeno" name="class2" pattern="[1-9a-zA-Z\s]+$" maxlength="5"></td>
							<td><input type="text" id="campo-pequeno" name="class3" pattern="[1-9a-zA-Z\s]+$" maxlength="5"></td>
						</tr>
					</table>
				</div>
			</div>
		
			<input type="submit" value="Cadastrar!" onclick="validarFormularioADM()" id="botao-grande">
		</form>
	</div>

</center>
</body>
</html>