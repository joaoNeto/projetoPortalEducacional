<html>
<head>
	<?php include("../iniciar-sessao_portal.php"); ?>
	<title>Escola Inteligente - Cadastro</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<script src="javascript.js" ></script>
</head>
<body>
<center>

	<div id="topo">
		<div id="topo-limite">
			<img src="../imagens/auto-falante.png" id="icone-auto-falante">
			<font id="texto-topo">
				Devido a política da empresa informe os dados abaixo por questões de segurança<br> para criar sua conta  agradecemos a compreensão
			</font>
			<a href="../index.php"><input type="submit" value="Voltar ao inicio" id="botao-medio"></a>
		</div>
	</div>
	
	<div id="cadastro-instituicao">
		<form method="post" name="formularioADM" action="cadastro.php?cadastro=funcionario">
			<font id="texto-cadastro">Cadastre seu funcionario</font>
			
			<div id="etapa-cadastro">
				<font id="titulo-etapa-cadastro">Sobre o seu funcionario</font><br>
				<div id="area-questionario">
					<input type="text"  placeholder="Nome completo*" required maxlength="50" name="nome_completo" pattern="[a-zA-Zãéíóô\s]+$" id="campo-grande">
					<input type="text" placeholder="Email*" name="email" pattern="[a-zA-Z0-9._%+-ãéíóô]+@[a-zA-Z0-9.-ãéíóô]+\.[a-z]{2,4}$"  maxlength="50" required id="campo-grande">
					<input type="text" placeholder="Telefone movel" pattern="[0-9()-]+$" name="tel_movel" maxlength="20" id="campo-medio">
					<input type="text" placeholder="Telefone fixo" pattern="[0-9()-]+$" name="tel_fixo" maxlength="20" id="campo-medio">
					<input type="password" placeholder="Senha*" pattern="[a-zA-Z0-9ãéíóô\s]+$" required name="senha" maxlength="20" id="campo-medio">
					<input type="password" placeholder="Repita sua senha*" pattern="[a-zA-Z0-9ãéíóô\s]+$" name="repetir_senha" required maxlength="20" id="campo-medio">
					<input type="text" placeholder="Como gostaria de ser chamado" pattern="[a-zA-Z0-9ãéíóô\s]+$" maxlength="12" name="ser_chamado" id="campo-grande">
				</div>
				<img src="../imagens/icone-perfil2.png" id="icone-etapa-cadastro-instituicao">
			</div>
			
			<div id="etapa-cadastro">
				<font id="titulo-etapa-cadastro">Localização do funcionario</font><br>
				<div id="area-questionario">
					<select id="campo-medio" name="estado" >
					<option>Estado</option>
					<option value="acre">Acre (AC)</option>
					<option value="alagoas">Alagoas (AL)</option>
					<option value="amapa">Amapá (AP)</option>
					<option value="amazonas">Amazonas (AM)</option>
					<option value="bahia">Bahia (BA)</option>
					<option value="ceara">Ceará (CE)</option>
					<option value="distrito federal">Distrito Federal (DF)</option>
					<option value="espirito santo">Espírito Santo (ES)</option>
					<option value="goias">Goiás (GO)</option>
					<option value="maranhao">Maranhão (MA)</option>
					<option value="mato grosso">Mato Grosso (MT)</option>
					<option value="mato grosso do sul">Mato Grosso do Sul (MS)</option>
					<option value="minas gerais">Minas Gerais (MG)</option>
					<option value="para">Pará (PA) </option>
					<option value="paraiba">Paraíba (PB)</option>
					<option value="parana">Paraná (PR)</option>
					<option value="pernambuco">Pernambuco (PE)</option>
					<option value="piaui">Piauí (PI)</option>
					<option value="rio de janeiro">Rio de Janeiro (RJ)</option>
					<option value="rio grande do norte">Rio Grande do Norte (RN)</option>
					<option value="rio grande do sul">Rio Grande do Sul (RS)</option>
					<option value="rondonia">Rondônia (RO)</option>
					<option value="roraima">Roraima (RR)</option>
					<option value="santa catarina">Santa Catarina (SC)</option>
					<option value="sao paulo">São Paulo (SP)</option>
					<option value="sergipe">Sergipe (SE)</option>
					<option value="tocantins">Tocantins (TO)</option>
					</select>
					<input type="text" placeholder="CEP" name="cep" pattern="[0-9]+$" maxlength="20" id="campo-medio">
					<input type="text" placeholder="Cidade" name="cidade" maxlength="50" pattern="[áãéíóôa-zA-Z\s]+$" id="campo-medio">
					<input type="text" placeholder="Bairro" name="bairro" pattern="[áãéíóôa-zA-Z\s]+$" maxlength="50" id="campo-medio">
					<input type="text" placeholder="Rua" name="rua" pattern="[áãéíóôa-zA-Z\s]+$"  maxlength="50" id="campo-medio">
					<input type="text" placeholder="Numero" name="numero" pattern="[0-9]+$" maxlength="5" id="campo-medio">			
					<input type="text" placeholder="Complemento" pattern="[a-zA-Z1-9\s]+$" name="complemento" maxlength="50" id="campo-grande">


					
					
				</div>
				<img src="../imagens/icone-localizacao.png" id="icone-etapa-cadastro-instituicao2">
			</div>
	
			<div id="etapa-cadastro">
				<font id="titulo-etapa-cadastro">Qual é o tipo de funcionario: <font id="tec-cor">professor</font><input type="radio" name="tipo_funcionario" value="professor" onclick="escolhaProfessor()"> <font id="tec-cor">outro funcionario</font><input type="radio" name="tipo_funcionario" value="outro funcionario" onclick="escolhaOutroFuncionario()"></font><br><br>

				
				<div id="area-questionario-professor">
					<input type="text" placeholder="matéria lessionada" pattern="[a-zA-Z1-9áéíóãõ\s]+$" name="materia_lessionada" maxlength="20" id="campo-grande">					
					<input type="text" placeholder="Salario* ex: 200" pattern="[0-9\s]+$" name="salario_professor" maxlength="20" id="campo-grande">					
					
					<table border="0" id="tabela_dias_a_lessionar">
						<tr>
							<td>dias de aula*:</td>
							<td>seg</td>
							<td>ter</td>
							<td>qua</td>
							<td>qui</td>
							<td>sex</td>
							<td>sab</td>
							<td>dom</td>
						</tr>					

					<input type="text" name="seguna" value="0" style="display:none;">
					<input type="text" name="terca" value="0" style="display:none;">
					<input type="text" name="quarta" value="0" style="display:none;">
					<input type="text" name="quinta" value="0" style="display:none;">			
					<input type="text" name="sexta" value="0" style="display:none;">
					<input type="text" name="sabado" value="0" style="display:none;">
					<input type="text" name="domingo" value="0" style="display:none;">
					
						<tr>
							<td></td>
							<td><input type="radio" name="segunda" value="1"></td>
							<td><input type="radio" name="terca" value="1"></td>
							<td><input type="radio" name="quarta" value="1"></td>
							<td><input type="radio" name="quinta" value="1"></td>
							<td><input type="radio" name="sexta" value="1"></td>
							<td><input type="radio" name="sabado" value="1"></td>
							<td><input type="radio" name="domingo" value="1"></td>
						</tr>
					</table>
					
					<br>
					
					<div id="tabela_dias_a_lessionar">
					
						<font id="fonte-lessionar">salas de aula lessionadas pelo professor(a):</font>

						<?php
						
							include("../conectar_banco.php");
						
							$sqlSala = "SELECT * FROM sala_de_aula WHERE nome_instituicao = '".$_SESSION["nome_instituicao"]."'";
							$resultadoSql = mysql_query($sqlSala);
							$contadorSala = 1;
							while($atributoSala = mysql_fetch_array($resultadoSql)){
								echo"
									<div id='area-radio-professor'>
										<input type='checkbox' name='sala".$contadorSala."' value='".$atributoSala["sala_de_aula"]."'>".$atributoSala["sala_de_aula"]."
									</div>
								";
								$contadorSala = $contadorSala + 1;
							}
							$contadorSala = $contadorSala - 1;
						?>
						
					</div>
					
					<input type='text' name='contadorSala' value='<?php echo $contadorSala; ?>' style="display:none;">
					
				</div>
				
				<div id="area-questionario-outro-funcionario">
					<input type="text" placeholder="nome do cargo*" pattern="[a-zA-Z\s]+$" name="nome_cargo" maxlength="20" id="campo-grande">					
					<input type="text" placeholder="salário*, ex: 200" pattern="[0-9\s]+$" name="salario" maxlength="5" id="campo-grande">					
					
					<font id="titulo-etapa-cadastro">Ativar algum recurso</font> <a href="../vantagens.html" id="subtitulo-etapa-cadastro">(quero saber mais...)</a><br>
					<table border="0" id="area-questionario-tabela">
						
						<input type="text" name="r_pe" value="0>" style="display:none;">
						<input type="text" name="r_sa" value="0" style="display:none;">
						<input type="text" name="r_ga" value="0" style="display:none;">
						<input type="text" name="r_gf" value="0" style="display:none;">
						<input type="text" name="r_bo" value="0" style="display:none;">
						<input type="text" name="r_po" value="0" style="display:none;">
						
							<tr> 
								<td id="texto-questionario-tabela">Portal educacional</td> <td id="area-caixa-selecao-tabela"><input type="checkbox" name="r_pe" value="1"></td>
								<td id="espaco-tabela"></td>
								<td id="texto-questionario-tabela">Gerenciar sala de aula</td> <td id="area-caixa-selecao-tabela"><input type="checkbox" name="r_sa" value="1"></td> 
							</tr>
							<tr> 
								<td id="texto-questionario-tabela">Gerenciar aluno</td> <td id="area-caixa-selecao-tabela"><input type="checkbox" name="r_ga" value="1"></td>
								<td id="espaco-tabela"></td>
								<td id="texto-questionario-tabela">Gerenciar funcionario</td> <td id="area-caixa-selecao-tabela"><input type="checkbox" name="r_gf" value="1"></td> 
							</tr>
							<tr> 
								<td id="texto-questionario-tabela">Biblioteca online</td> <td id="area-caixa-selecao-tabela"><input type="checkbox" name="r_bo" value="1"></td>
								<td id="espaco-tabela"></td>
								<td id="texto-questionario-tabela"></td> <td id="area-caixa-selecao-tabela"></td> 
							</tr>
						
					</table>
					
				</div>
				
				<img src="../imagens/icone-perfil2.png" id="icone-etapa-cadastro-instituicao">
				<!--icone de pessoa trabalhando-->
			</div>
		
			<input type="submit" value="Cadastrar!" onclick="validarFormularioADM()" id="botao-grande">
		</form>
	</div>

</center>
</body>
</html>