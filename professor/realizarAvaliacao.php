<?php include("topoProfessor.php"); ?>

<div id="area-direita">

</div>

<div id="conteudo">
<center>

	<div id="topico-conteudo">
		<div id="titulo-topico-conteudo">
		Realizar avaliação
		</div>
	</div>
	
	<table style="float:left;">
		<tr>
			<td><font id="fonteSubTituloAvaliacao">Para quem se destina a avaliação?</font></td>
			<td><input type="radio" onclick="escolhaAvaliacaoEscola()" value="escola" name="EscolhaQuemAvaliar"></td>
			<td id="fonteTipoAvaliacao">Escola</td>
			<td><input type="radio" onclick="escolhaAvaliacaoSala()" value="sala" name="EscolhaQuemAvaliar"></td>
			<td id="fonteTipoAvaliacao">Sala de aula</td>
			<td><input type="radio" onclick="escolhaAvaliacaoAluno()" value="aluno"  name="EscolhaQuemAvaliar"></td>
			<td id="fonteTipoAvaliacao">Aluno</td>
		</tr>
	</table>
	
	<div id="SelecaoTipoAvaliacaoEscola">
		<form method="POST" action="realizarQuestoes.php">
		
			<table border="0" style="float:left;">
				<tr>
					<td><input type="text" placeholder="Digite o titulo do teste" name="titulo" id="campo-texto-grande"></td>
				</tr>
				<tr><td></td></tr>
				<tr>
					<td><font id="fonteSubTituloAvaliacao2">Digite a quantidade de questões:</font><input type="text" name="qtdQuestoes" placeholder="ex:10" id="campo-texto-mini"></td>
				</tr>
				<tr><td></td></tr>
				<tr>
					<td><input type="submit" value="Pronto!" id="botao-medio"></td>
				</tr>		
			</table>
		</form>
	</div>
	
	<div id="SelecaoTipoAvaliacaoSala">
		<form method="POST" action="realizarQuestoes.php">	
			<table border="0" style="float:left;">
				<tr>
					<td><input type="text" placeholder="Digite o titulo do teste" name="titulo" id="campo-texto-grande"></td>
				</tr>
				<tr><td></td></tr>
				<tr>
					<td>
						<select id="campo-texto-grande" name="sala_de_aula" >
							<option value="nao vinculado">Vincule aluno a alguma sala</option>
							<?php

							include("../conectar_banco.php");
							$sql = "SELECT * FROM sala_de_aula WHERE nome_instituicao = '".$_SESSION["nome_instituicao"]."'";
							$result = mysql_query($sql);
							while($atributo = mysql_fetch_array($result)){
								$sala = $atributo["sala_de_aula"];
								echo "<option value='$sala'>$sala</option>";
							}
							?>
						</select>			
					</td>
				</tr>			
				<tr><td></td></tr>
				<tr>
					<td><font id="fonteSubTituloAvaliacao2">Digite a quantidade de questões:</font><input type="text" name="qtdQuestoes" placeholder="ex:10" id="campo-texto-mini"></td>
				</tr>
				<tr><td></td></tr>
				<tr>
					<td><input type="submit" name="" value="Pronto!" id="botao-medio"></td>
				</tr>
			</table>
		</form>
		
	</div>
	
	<div id="SelecaoTipoAvaliacaoAluno">
		<form method="POST" action="realizarQuestoes.php">	
			<table border="0" style="float:left;">
				<tr>
					<td><input type="text" placeholder="Digite o titulo do teste" name="titulo" id="campo-texto-grande"></td>
				</tr>
				<tr><td></td></tr>
				<tr>
					<td><input type="text" placeholder="Digite o email do aluno" name="titulo" id="campo-texto-grande"></td>
				</tr>			
				<tr><td></td></tr>
				<tr>
					<td><font id="fonteSubTituloAvaliacao2">Digite a quantidade de questões:</font><input type="text" name="qtdQuestoes" placeholder="ex:10" id="campo-texto-mini"></td>
				</tr>
				<tr><td></td></tr>
				<tr>
					<td><input type="submit" name="" value="Pronto!" id="botao-medio"></td>
				</tr>
			</table>	
		</form>
	</div>

</center>
</div>


<?php include("rodapeProfessor.php"); ?>