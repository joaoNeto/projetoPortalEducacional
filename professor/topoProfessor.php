<?php 
include("../iniciar-sessao_portal.php ");
?>

<html>
<head>
	<title>Professor <?php echo $_SESSION["ser_chamado"]; ?></title>
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<script src="javascript.js" ></script>	
</head>
<body>
<center>
<div id="topo">

	<div id="menu-topo">
		<div id="topico-menu">
			<a href="login.php" id="texto-topico-menu">Inicio</a>
		</div>		
		<div id="topico-menu">
			<a href="AvaliacaoOnline.php" id="texto-topico-menu">Avaliação online</a>
		</div>
		<div id="topico-menu">
			<a href="Alunos.php" id="texto-topico-menu">Aluno</a>
		</div>			
		<div id="topico-menu">
			<a href="SalaDeAula.php" id="texto-topico-menu">Sala de aula</a>
		</div>
		<div id="topico-menu">
			<a href="mensagem.php" id="texto-topico-menu">Mensagens</a>
		</div>		
		
	</div>
	
	<div id="lado-direito-topo">
		<div id="cima">
		<?php echo $_SESSION["ser_chamado"] ?>
		</div>
		<div id="baixo">
			<a href="configuracaoProfessor.php"><img src="../imagens/icone-configuracao.png" id="icone-pequeno"></a>
			<a href="../validar-login.php?acao=sairDoPortal"><img src="../imagens/icone-desligar.png" id="icone-pequeno"></a>
		</div>		
	</div>	
	
</div>

<div id="area-perfil">
	<div id="area-foto" style="background-image:url('<?php echo $_SESSION["foto_perfil"]; ?>');">
	</div>
	<font id="texto-materia-lessionada">Professor de <?php echo $_SESSION["materia_lessionada"]; ?></font>
</div>

