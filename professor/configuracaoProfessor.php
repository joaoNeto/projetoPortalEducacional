<?php include("topoProfessor.php"); ?>


	<div id="conteudo">
		<div id="topico-conteudo">
		
			<div id="titulo-topico-conteudo">
			Configuração
			</div>
			<br><br><br>
			<form method="post" action="CRUDPROFESSOR.php?acao=mudarProfessor" enctype="multipart/form-data">
			<div id="etapa-cadastro">
				<font id="titulo-etapa-cadastro">Sobre voce</font><br>
				<div id="area-questionario">
					<input type="text"  value="<?php echo $_SESSION["nome_completo"]; ?>" placeholder="nome completo" required maxlength="200" name="nome_completo" pattern="[a-zA-Zãéíóô\s]+$" id="campo-grande">
					<input type="text" value="<?php echo $_SESSION["email"]; ?>" placeholder="email" name="email" pattern="[a-zA-Z0-9._%+-ãéíóô]+@[a-zA-Z0-9.-ãéíóô]+\.[a-z]{2,4}$"  maxlength="150" required id="campo-grande">
					<input type="text" value="<?php echo $_SESSION["telefone_movel"]; ?>" placeholder="telefone movel" pattern="[0-9()-]+$" name="tel_movel" maxlength="15" id="campo-medio">
					<input type="text" value="<?php echo $_SESSION["telefone_fixo"]; ?>" placeholder="telefone fixo" pattern="[0-9()-]+$" name="tel_fixo" maxlength="15" id="campo-medio">
					<input type="text" value="<?php echo $_SESSION["ser_chamado"]; ?>" placeholder="como gostaria de ser chamado" pattern="[a-zA-Z0-9ãéíóô\s]+$" required maxlength="12" name="ser_chamado" id="campo-grande">
					<font id="texto-inserir-imagem">Modifique a foto para o seu perfil:</font><input type="file" value="<?php echo $_SESSION["foto_perfil"]; ?>" name="foto_perfil" id="campo-grande-foto">
				</div>
			</div>
			
			<div id="etapa-cadastro">
				<font id="titulo-etapa-cadastro">Localização da escola</font><br>
				<div id="area-questionario">
					<input type="text" value="<?php echo $_SESSION["estado"]; ?>" name="estado" placeholder="estado" required pattern="[a-zA-Z ()]+$" maxlength="20" id="campo-medio">
					<input type="text" value="<?php echo $_SESSION["cep"]; ?>" name="cep" pattern="[0-9]+$" placeholder="cep" required maxlength="20" id="campo-medio">
					<input type="text" value="<?php echo $_SESSION["cidade"]; ?>" name="cidade" required placeholder="cidade" maxlength="50" pattern="[áãéíóôa-zA-Z\s]+$" id="campo-medio">
					<input type="text" value="<?php echo $_SESSION["bairro"]; ?>" name="bairro" pattern="[áãéíóôa-zA-Z\s]+$" placeholder="bairro" maxlength="50" id="campo-medio">
					<input type="text" value="<?php echo $_SESSION["rua"]; ?>" name="rua" pattern="[áãéíóôa-zA-Z\s]+$" placeholder="rua"  maxlength="50" id="campo-medio">
					<input type="text" value="<?php echo $_SESSION["numero"]; ?>" name="numero" pattern="[0-9]+$" maxlength="5" placeholder="numero" id="campo-medio">			
					<input type="text" value="<?php echo $_SESSION["complemento"]; ?>" pattern="[a-zA-Z1-9\s]+$" placeholder="complemento" name="complemento" maxlength="50" id="campo-grande">
				</div>
			</div>	
		
		<input type="submit" value="Alterar" id="botao-grande">
		</form>
		<a href="alterarSenha.php">
			<div id="botao-grande2">
				Alterar senha
			</div>
		</a>
		<a href="excluirConta.php">
			<div id="botao-grande2">
				excluir conta
			</div>
		</a>
		
		</div>
	</div>



<?php include("rodapeProfessor.php"); ?>