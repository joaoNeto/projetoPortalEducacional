<?php include("topoAluno.php"); ?>


	<div id="conteudo">
		<div id="topico-conteudo">
		
			<div id="titulo-topico-conteudo">
			Alterar senha
			</div>
			<br><br><br>
			<form method="post" action="CRUDAluno.php?acao=mudarSenhaAluno">
			
			<div id="etapa-cadastro">
				<font id="titulo-etapa-cadastro">Altere sua senha</font><br>
				<div id="area-questionario">
					<input type="password" placeholder="digite sua senha antiga" required maxlength="20" name="senha_antiga" pattern="[0-9a-zA-Zãéíóô\s]+$" id="campo-grande">
					<input type="password" placeholder="digite sua nova senha" name="nova_senha" pattern="[0-9a-zA-Zãéíóô\s]+$"  maxlength="20" required id="campo-grande">
					<input type="password" placeholder="redigite sua nova senha" name="nova_senha_redigitada" pattern="[0-9a-zA-Zãéíóô\s]+$"  maxlength="20" required id="campo-grande">
				</div>
			</div>
			
			<input type="submit" value="Alterar" id="botao-grande">
			
			</form>
			
			<a href="configuracaoAluno.php">
				<div id="botao-grande2">
					Voltar Configuração
				</div>
			</a>

		
		</div>
	</div>



<?php include("rodapeAluno.php"); ?>