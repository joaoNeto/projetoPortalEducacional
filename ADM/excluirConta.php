
<?php require("topoADM.php"); ?>

	<div id="conteudo">
		<form method="post" action="CRUDADM.php?acao=excluir">
			<font id="titulo-configuracao-adm">Delete sua conta</font>
			
			<div id="etapa-cadastro">
				<font id="titulo-etapa-cadastro">Vamos sentir saudades... :(</font><br>
					<div id="area-questionario">
						<input type="text" placeholder="Digite seu email" name="email" pattern="[a-zA-Z0-9._%+-ãéíóô]+@[a-zA-Z0-9.-ãéíóô]+\.[a-z]{2,4}$"  maxlength="100" required id="campo-grande">
						<input type="password" name="senha" placeholder="Digite sua senha" pattern="[a-zA-Z0-9ãéíóô\s]+$" required maxlength="20" id="campo-grande">
					</div>
				<font id="titulo-etapa-cadastro" style="color:red;">*Ao Deletar a conta todas suas informações e da sua escola são excluidos automaticamente</font><br>
			</div>
			
			<input type="submit" value="excluir" id="botao-grande">
			
			<a href="configuracaoADM.php">
				<div id="botao-grande2">
					Voltar as configurações
				</div>
			</a>
			
		</form>
	</div>

	<?php require("rodapeADM.php"); ?>