
<?php require("topoADM.php"); ?>

	<div id="conteudo">
		<form method="post" action="CRUDADM.php?acao=alterar" enctype="multipart/form-data">
		<font id="titulo-configuracao-adm">Configuração</font>
		
		<div id="etapa-cadastro">
			<font id="titulo-etapa-cadastro">Sobre voce</font><br>
			<div id="area-questionario">
				<input type="text"  value="<?php echo $_SESSION["nome_completo"]; ?>" placeholder="nome completo" required maxlength="200" name="nome_completo" pattern="[a-zA-Zãéíóô\s]+$" id="campo-grande">
				<input type="text" value="<?php echo $_SESSION["email"]; ?>" placeholder="email" name="email" pattern="[a-zA-Z0-9._%+-ãéíóô]+@[a-zA-Z0-9.-ãéíóô]+\.[a-z]{2,4}$"  maxlength="150" required id="campo-grande">
				<input type="text" value="<?php echo $_SESSION["tel_movel"]; ?>" placeholder="telefone movel" pattern="[0-9()-]+$" name="tel_movel" maxlength="15" id="campo-medio">
				<input type="text" value="<?php echo $_SESSION["tel_fixo"]; ?>" placeholder="telefone fixo" pattern="[0-9()-]+$" name="tel_fixo" maxlength="15" id="campo-medio">
				<input type="text" value="<?php echo $_SESSION["ser_chamado"]; ?>" placeholder="como gostaria de ser chamado" pattern="[a-zA-Z0-9ãéíóô\s]+$" required maxlength="12" name="ser_chamado" id="campo-grande">
				<font id="texto-inserir-imagem">Modifique a foto para o seu perfil:</font><input type="file" value="<?php echo $_SESSION["foto_perfil"]; ?>" name="foto_perfil" id="campo-grande-foto">
			</div>
		</div>
		
		<div id="etapa-cadastro">
			<font id="titulo-etapa-cadastro">Sobre sua escola</font><br>
			<div id="area-questionario">
				<input type="text" value="<?php echo $_SESSION["nome_instituicao"]; ?>" placeholder="nome da instituicao" pattern="[a-zA-Zãéíóô\s]+$" required name="nome_instituicao" maxlength="50" id="campo-grande">
				<input type="text" value="<?php echo $_SESSION["cnpj"]; ?>" name="cnpj" placeholder="cnpj" required pattern="[0-9]+$" maxlength="25" id="campo-grande">
				<input type="text" value="<?php echo $_SESSION["inscr_estadual"]; ?>"  name="inscr_estadual" placeholder="inscrição estadual" required pattern="[0-9]+$" maxlength="20" required id="campo-medio">
				<input type="text" value="<?php echo $_SESSION["data_fundacao"]; ?>" name="data_fundacao" placeholder="data de fundação" maxlength="10" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}$" title="o formato deve ser: dd/mm/aaa" id="campo-medio">
				<input type="text" value="<?php echo $_SESSION["telefone_movel"]; ?>" name="telefone_movel" placeholder="telefone movel" pattern="[0-9()-]+$" maxlength="15" id="campo-medio">
				<input type="text" value="<?php echo $_SESSION["telefone_fixo"]; ?>" name="telefone_fixo" pattern="[0-9()-]+$" placeholder="telefone fixo" maxlength="15" id="campo-medio">
			</div>
		</div>	

		<div id="etapa-cadastro">
			<font id="titulo-etapa-cadastro">Localização da escola</font><br>
			<div id="area-questionario">
				<input type="text" value="<?php echo $_SESSION["estado"]; ?>" name="estado" placeholder="estado" required pattern="[a-zA-Z ()]+$" maxlength="20" id="campo-medio">
				<input type="text" value="<?php echo $_SESSION["cep"]; ?>" name="cep" pattern="[0-9]+$" placeholder="cep" required maxlength="20" id="campo-medio">
				<input type="text" value="<?php echo $_SESSION["cidade"]; ?>" name="cidade" required placeholder="cidade" maxlength="50" pattern="[áãéíóôa-zA-Z\s]+$" id="campo-medio">
				<input type="text" value="<?php echo $_SESSION["bairro"]; ?>" name="bairro" pattern="[áãéíóôa-zA-Z\s]+$" placeholder="bairro" maxlength="50" id="campo-medio">
				<input type="text" value="<?php echo $_SESSION["rua"]; ?>" name="rua" pattern="[áãéíóôa-zA-Z\s]+$" placeholder="rua"  maxlength="50" id="campo-medio">
				<input type="text" value="<?php echo $_SESSION["numero"]; ?>" name="numero" pattern="[0-9]+$" maxlength="5" placeholder="numero" id="campo-medio">			
				<input type="text" value="<?php echo $_SESSION["complemento"]; ?>" pattern="[a-zA-Z1-9\s]+$" placeholder="complemento" name="complemento" maxlength="50" id="campo-grande">
			</div>
		</div>		

		<div id="etapa-cadastro">
			<font id="titulo-etapa-cadastro">Desativar algum recurso</font> <a href="../vantagens.html" id="subtitulo-etapa-cadastro">(quero saber mais...)</a><br>
			
			<table border="0" id="area-questionario-tabela">
			
			<input type="text" name="r_pe" value="<?php echo $_SESSION["r_pe"]; ?>" style="display:none;">
			<input type="text" name="r_sa" value="<?php echo $_SESSION["r_sa"]; ?>" style="display:none;">
			<input type="text" name="r_ga" value="<?php echo $_SESSION["r_ga"]; ?>" style="display:none;">
			<input type="text" name="r_gf" value="<?php echo $_SESSION["r_gf"]; ?>" style="display:none;">
			<input type="text" name="r_bo" value="<?php echo $_SESSION["r_bo"]; ?>" style="display:none;">
			<input type="text" name="r_po" value="<?php echo $_SESSION["r_po"]; ?>" style="display:none;">
			
				<tr> 
					<td id="texto-questionario-tabela">Portal educacional</td> <td id="area-caixa-selecao-tabela"><input type="checkbox" name="r_pe" value="0"></td>
					<td id="espaco-tabela"></td>
					<td id="texto-questionario-tabela">Gerenciar sala de aula</td> <td id="area-caixa-selecao-tabela"><input type="checkbox" name="r_sa" value="0"></td> 
				</tr>
				<tr> 
					<td id="texto-questionario-tabela">Gerenciar aluno</td> <td id="area-caixa-selecao-tabela"><input type="checkbox" name="r_ga" value="0"></td>
					<td id="espaco-tabela"></td>
					<td id="texto-questionario-tabela">Gerenciar funcionario</td> <td id="area-caixa-selecao-tabela"><input type="checkbox" name="r_gf" value="0"></td> 
				</tr>
				<tr> 
					<td id="texto-questionario-tabela">Biblioteca online</td> <td id="area-caixa-selecao-tabela"><input type="checkbox" name="r_bo" value="0"></td>
					<td id="espaco-tabela"></td>
					<td id="texto-questionario-tabela">Planilhas de orçamento</td> <td id="area-caixa-selecao-tabela"><input type="checkbox" name="r_po" value="0"></td> 
				</tr>
				
			</table>
			
		</div>	
		
		<input type="submit" value="Alterar" id="botao-grande">
		<a href="alterarSenha.php">
			<div id="botao-grande2">
				Alterar senha
			</div>
		</a>
		<a href="excluirConta.php">
			<div id="botao-grande2">
				excluir conta
			</div>
		</a>
		</form>
	</div>

	<?php require("rodapeADM.php"); ?>