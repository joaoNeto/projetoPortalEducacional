
<?php require("topoADM.php"); ?>

	<div id="conteudo">
		<form method="post" action="CRUDADM.php?acao=alterarSenha">
			<font id="titulo-configuracao-adm">Altere sua senha</font>
			<div id="etapa-cadastro">
				<div id="area-questionario">
					<input type="password" name="senha_antiga" placeholder="Digite sua senha antiga" pattern="[a-zA-Z0-9ãéíóô\s]+$" required  maxlength="20" id="campo-grande">
					<input type="password" name="nova_senha" placeholder="Digite sua nova senha" pattern="[a-zA-Z0-9ãéíóô\s]+$" required maxlength="20" id="campo-grande">
					<input type="password" name="nova_senha_redigitada" placeholder="Redigite sua nova senha" pattern="[a-zA-Z0-9ãéíóô\s]+$" required  maxlength="20" id="campo-grande">
				</div>
			</div>
			
			<input type="submit" value="Alterar" id="botao-grande">
			<a href="configuracaoADM.php">
			<div id="botao-grande2">
				Voltar as configurações
			</div>
			</a>
		</form>
	</div>

	<?php require("rodapeADM.php"); ?>